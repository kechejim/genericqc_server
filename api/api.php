<?php
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Slim Framework
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
require 'Slim/Slim.php';
\Slim\Slim::registerAutoloader();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
// Redbean PHP
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
require_once('rb.php');
require_once('rbsetup.php');

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  PHP Globals
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
ini_set('memory_limit', '-1');
error_reporting(E_ALL);
ini_set('display_errors', TRUE);
ini_set('display_startup_errors', TRUE);
define('EOL',(PHP_SAPI == 'cli') ? PHP_EOL : '<br />');
date_default_timezone_set('Asia/Hong_Kong');
session_start();

///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//
//  Slim routes
//
///////////////////////////////////////////////////////////////////////////////////////////////////////////////////
$app = new \Slim\Slim();

$authenticate = function ($app) {
    return function () use ($app) {
        if (!isset($_SESSION['uid'])) {
            $app->redirect('../login');
        }
    };
};

//
//	Generic table with paging
//
function addWhere($where, $clause) {
	if ($where == "") {$where = " where ";} else {$where = $where . " and ";};
	return $where . $clause;
}
function addWhereOr($where, $clause) {
	if ($where == "") {$where = " where ";} else {$where = $where . " or ";};
	return $where . $clause;
}

$app->get('/', function () use ($app) {

	$res ['data'] = 'test';
	echo json_encode($res);
});

$app->get('/getappinfo', function () use ($app) {

	$request = $app->request();
	$body = $request->getBody();

	$appname = $request->get('appname'). '.json';

	$data = file_get_contents(dirname(__FILE__).'/data/'.$appname);
	$json = json_decode($data, true);

	$res['pagelist'] = $json;
	
	echo json_encode($res);
});

$app->post('/submitreport', function () use($app) {
			
	$report = $app->request()->getBody();
	$data = json_decode($app->request()->getBody());

	$json = $data->data; 
	$appname = $data->app;

	file_put_contents(dirname(__FILE__).'/data/'. $appname.'.json', json_encode($json));
	
	echo json_encode($report);

});
/*
$app->get('/:table', function ($table) use ($app) {

	//
	//	Handle paging
	//
	$pagesize=$_REQUEST['pagesize'];
	$start=0;
	$currentPage=1;
	$where = "";
	$request = $app->request();
	$body = $request->getBody();
	$input = json_decode($body); 
	if($request->get('page')) {
		$currentPage = $request->get('page');
		$start = ($pagesize * ($currentPage -1));
	}

	//
	//	Fields selection
	//
	$fields = "*";


	//
	// Filter conditions here
	//
	if (isset($_REQUEST['inspection_date'])) { 
		$where = addWhere($where, "length(inspection_date) > 4");
	};

	if (isset($_REQUEST['sortby'])) { $where =  $where . " order by " . $_REQUEST['sortby'];};
	if (isset($_REQUEST['sortdir']) && $_REQUEST['sortdir'] == 'desc') { $where =  $where . " desc"; };
	
	//
	//	Page Size
	//
	$limit = ' limit ' . $start . ',' . $pagesize;
	if (isset($_REQUEST['export']))
	{
		$limit = " limit 64000";
	}


	//
	//	Summary columns here
	//
	$summcols = "";

	$statsql = 'select count(*) as count ' . $summcols . ' from ' . $table . ' ' . $where;
	$sql = 	'select ' . $fields . ' from ' . $table . ' '
	. $where
	. $limit;


	//echo $sql;

	$stat = R::getAll ($statsql);

	if (sizeof($stat) == 0) {
		$data['items']=0;
		echo json_encode($data);
		exit;
	}
	$all = R::getAll ($sql);


	//
	//	Paging columns here
	//	
	$data['itemcount']=sizeof($all);
	$data['currentpage']=$currentPage*1;
	$data['totalitems']=$stat[0]['count'];
	$data['totalpages']=ceil($stat[0]['count'] / $pagesize);

	//
	//	Stuff data here
	//
	$data['items']=$all;

	//
	//	Export code here
	//
	if (isset($_REQUEST['export']))
	{
		download_send_headers($table . "_export_" . date("Y-m-d") . ".csv");
		echo array2csv($data['items']);
	} else {
		$app->response()->header('Content-Type', 'application/json');
		if($table=="users"){
			if($_SESSION['level'] == 2){
				echo json_encode($data);
			}else{
				echo "errorprivileges";
			}
		}else{
			echo json_encode($data);
		}
	}
});*/

$app->run();

exit;


?> 
